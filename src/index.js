import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Calculator extends React.Component{
    value1='';
    constructor(props){
        super(props);
        this.state = {
            currentValue:'0',
            total : '',
            operators : ['+','-','/','*'],
        }
        this.value1 = '';
        this.handleClicks = this.handleClicks.bind(this);
        this.handleallclear = this.handleallclear.bind(this);
        this.handleclear = this.handleclear.bind(this);
        this.handleOperations = this.handleOperations.bind(this);
    }

    handleClicks(e){
        let value = '';
        let count = 0;
        let decimalPoint = ""+this.state.currentValue;
        let a = ""+decimalPoint.split('').map((e)=>{
            if(e === '.')
            count++;
        });
        if(decimalPoint.length < 26 && (count < 1 || e.target.value !== '.' ) && ((this.state.currentValue)[0] !== '0' ||  e.target.value !== '0')  ){ 
            if(this.state.operators.indexOf(e.target.value) !== -1){
                this.value1 = e.target.value;
            }else{
                if(this.state.operators.indexOf(this.value1) !== -1){
                    this.value1 = e.target.value;
                }else
                    this.value1 += (e.target.value);
            }        
            this.setState ({
                currentValue : this.value1,
                total: this.state.total+e.target.value,
            })
        }    
            let tvalue = this.state.total;
            if(tvalue.split('').includes('=')){
                if(this.state.operators.includes(e.target.value)){
                    value = this.state.currentValue+e.target.value;
                }else{
                    value = e.target.value;
                }
                this.setState ({
                    currentValue : e.target.value,
                    total: value,
                });
                this.value1=e.target.value;
            }        
    }
    handleallclear(e){
        this.setState ({
            currentValue : '0',
            total: '',
        })
        this.value1 = '';
    }

    handleclear(){
        let cvalue = this.state.total;
        let a = cvalue.split('');
        a.splice(cvalue.length-1, 1);
        this.setState ({
            currentValue : '0',
            total: a.join('')
        })
        this.value1 = '';
    }

    handleOperations(e){
        let result = 0;
        let multiply = 1;
        let cal = '';
        let arr = [];
        let operator = [];
        var wholevalue = this.state.total;
        if(!wholevalue.split('').includes('=')){
        for(let i=0; i<wholevalue.length; i++){
            if(this.state.operators.includes(wholevalue[i]) && this.state.operators.includes(wholevalue[i+1]) ){
                wholevalue = wholevalue.split('');
                wholevalue.splice(i,1);
                wholevalue = wholevalue.join('');
                i=0;
            }
        }
        for(let i=0; i<=wholevalue.length; i++){
            if(this.state.operators.indexOf(wholevalue[i]) !== -1){
                if((cal.split('').includes('.'))){
                    arr.push(parseFloat(cal));
                }else{
                    arr.push(parseInt(cal));
                }
                operator.push(wholevalue[i]);
                cal = '';
            }else{
                cal += wholevalue[i];
            }
        }


        if((cal.split('').includes('.'))){
            arr.push(parseFloat(cal));
        }else{
            arr.push(parseInt(cal));
        }

        while(operator.includes('*')){
            var index1 = operator.indexOf('*');
            multiply = arr[index1]*arr[index1+1];
            arr.splice(index1,1);
            arr[index1] = multiply;
            operator.splice(index1,1);
        }
        while(operator.includes('/')){
            var divideIndex = operator.indexOf('/');
            result = arr[divideIndex]/arr[divideIndex+1];
            arr.splice(divideIndex,1);
            arr[divideIndex] = result;
            operator.splice(divideIndex,1);
        }
        while(operator.includes('+')){
            var divideIndex = operator.indexOf('+');
            result = arr[divideIndex]+arr[divideIndex+1];
            arr.splice(divideIndex,1);
            arr[divideIndex] = result;
            operator.splice(divideIndex,1);
        }
        while(operator.includes('-')){
            var divideIndex = operator.indexOf('-');
            result = arr[divideIndex]-arr[divideIndex+1];
            arr.splice(divideIndex,1);
            arr[divideIndex] = result;
            operator.splice(divideIndex,1);
        }
        if(arr.length === 1)
            result = arr[0];

        this.setState ({
            currentValue : result,
            total: this.state.total+"= "+result
        })
    }
    }
    render(){
        return(
            <div id="mainContainer">
                <div id="displayWrap"><p id="display1">{this.state.total}</p></div>
                <div id="displayWrap"><p id="display">{this.state.currentValue}</p></div>
                <div id="buttons">
                    <button onClick={this.handleallclear} value="ac"  id="Allclear">AC</button>
                    <button onClick={this.handleclear} value="c"  id="clear">C</button>
                    <button onClick={this.handleClicks} value="/" id="divide">/</button>
                    <button onClick={this.handleClicks} value="*" id="multiply">x</button>
                    <button onClick={this.handleClicks} value="7" id="seven">7</button>
                    <button onClick={this.handleClicks} value="8" id="eight">8</button>
                    <button onClick={this.handleClicks} value="9" id="nine">9</button>
                    <button onClick={this.handleClicks} value="-" id="subtract">-</button>
                    <button onClick={this.handleClicks} value="4" id="four">4</button>
                    <button onClick={this.handleClicks} value="5" id="five">5</button>
                    <button onClick={this.handleClicks} value="6" id="six">6</button>
                    <button onClick={this.handleClicks} value="+" id="add">+</button>
                    <button onClick={this.handleClicks} value="1" id="one">1</button>
                    <button onClick={this.handleClicks} value="2" id="two">2</button>
                    <button onClick={this.handleClicks} value="3" id="three">3</button>
                    <button onClick={this.handleOperations} value="=" id="equals">=</button>
                    <button onClick={this.handleClicks} value="0" id="zero">0</button>
                    <button onClick={this.handleClicks} value="." id="decimal">.</button>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<Calculator />, document.getElementById('root'));